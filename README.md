Example service behind the proxy: 

```
services:
  web:
    image: nginx:latest
    container_name: domain_nginx
    restart: always
    expose:
      - 80
    networks:
      - default
      - hosting-with-docker_nginx_reverse_proxy
    environment:
      - VIRTUAL_HOST=${VIRTUAL_HOST}
      - LETSENCRYPT_HOST=${VIRTUAL_HOST}
      - LETSENCRYPT_EMAIL=${LETSENCRYPT_EMAIL}

networks:
  hosting-with-docker_nginx_reverse_proxy:
    external: true
  default:
    external: false
```